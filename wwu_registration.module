<?php

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\webform\WebformInterface;

/**
 * Implements hook_node_update().
 *
 * If the seating capacity is increased or reset, re-open the webform for
 * submissions.
 */
function wwu_registration_node_update(EntityInterface $entity) {
  if ($entity->bundle() === 'registration') {
    $referencedEntities = $entity->get('field_registration')->referencedEntities();
    $webform = array_pop($referencedEntities);
    $seating_capacity = (int) $entity->field_seating_capacity->value;

    if ($seating_capacity > 0 && $webform->status() !== WebformInterface::STATUS_OPEN) {
      $webform->setStatus(WebformInterface::STATUS_OPEN)->save();
    }
  }
}

/**
 * Implements hook_entity_bundle_field_info_alter().
 */
function wwu_registration_entity_bundle_field_info_alter(&$fields, EntityTypeInterface $entity_type, $bundle) {
  if ($bundle === 'registration') {
    if (isset($fields['field_registration'])) {
      $fields['field_registration']->addConstraint('WebformHasSeatsElement', []);
    }
  }
}
