<?php

namespace Drupal\wwu_registration\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the WebformHasSeatsElement constraint.
 */
final class WebformHasSeatsElementValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {
      $reference = $item->get('entity');
      $adapter = $reference->getTarget();
      $webform = $adapter->getValue();
      $seats = $webform->getElement('seats');

      if (is_null($seats)) {
        $this->context->addViolation($constraint->missingElement);
      }
      elseif ($seats['#type'] !== 'number') {
        $this->context->addViolation($constraint->invalidElement, ['%value' => $seats['#type']]);
      }
    }
  }

}
