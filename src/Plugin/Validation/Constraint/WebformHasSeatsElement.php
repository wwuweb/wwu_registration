<?php

namespace Drupal\wwu_registration\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Requires a Webform reference to have an element with the "seats" name.
 *
 * @Constraint(
 *   id = "WebformHasSeatsElement",
 *   label = @Translation("Webform reference required to have a 'seats' element.", context="Validation"),
 *   type = "entity"
 * )
 */
final class WebformHasSeatsElement extends Constraint {

  public $missingElement = 'You must select a Webform with an element with the key <em>seats</em>.';

  public $invalidElement = 'The <em>seats</em> element must be of the type <em>number</em>. The field in the form you selected is of the type %value.';

}
