<?php

namespace Drupal\wwu_registration\Plugin\WebformHandler;

use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Registration handler.
 *
 * @WebformHandler(
 *   id = "registration",
 *   label = @Translation("Registration"),
 *   category = @Translation("Registration"),
 *   description = @Translation("Registration webform submission handler."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_IGNORED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
final class RegistrationHandler extends WebformHandlerBase {

  /**
   * @var string Lock to avoid contention over webform submissions.
   */
  private const REGISTRATION_HANDLER_LOCK = 'WWU_REGISTRATION_HANDLER_LOCK';

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $lock = \Drupal::lock();

    while (!$lock->acquire(self::REGISTRATION_HANDLER_LOCK)) {
      $lock->wait(self::REGISTRATION_HANDLER_LOCK);
    }

    $source_entity = $webform_submission->getSourceEntity();
    $seating_capacity = (int) $source_entity->field_seating_capacity->value;
    $maximum_reservable = (int) $source_entity->field_max_seats_per_reservation->value;
    $seating_reserved = (int) $form_state->getValue('seats');

    if ($seating_capacity == 0) {
      $form_state->setErrorByName('seats', $this->t('There are no seats available.'));
    }
    else if ($seating_reserved > $seating_capacity) {
      $form_state->setErrorByName('seats', $this->t('You have reserved more seats than are currently available.'));
    }
    else if ($maximum_reservable && $seating_reserved > $maximum_reservable) {
      $form_state->setErrorByName('seats', $this->t('You have reserved more than the maximum allowable seats.'));
    }
    else {
      $source_entity->set('field_seating_capacity', $seating_capacity - $seating_reserved);
      $source_entity->save();
    }

    $lock->release(self::REGISTRATION_HANDLER_LOCK);
  }

  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    if ($webform_submission->getState() == WebformSubmissionInterface::STATE_COMPLETED) {
      $source_entity = $webform_submission->getSourceEntity();
      $seating_capacity = (int) $source_entity->field_seating_capacity->value;

      if ($seating_capacity == 0) {
        $webform_submission->getWebform()->setStatus(WebformInterface::STATUS_CLOSED)->save();
      }
    }
  }

}
